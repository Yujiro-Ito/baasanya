﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GetLocation : MonoBehaviour {

	private float _latitude = 0;
	private float _longitude = 0;
	private bool _enabled = true;
	public float Latitude { get { return _latitude; } }
	public float Longitude { get { return _longitude; } }
	public bool Enabled{ get{ return _enabled; } }
	private bool _getNow;

	void Start(){
		StartCoroutine ("Location");
	}

	void Update(){
		if (_getNow = false) {
			StartCoroutine("Location");
		}
	}

	//位置情報をとりましょう。
	IEnumerator Location(){
		_getNow = true;
		Debug.Log ("Position get start");
		//もし、位置情報を取る機器がなければ諦める
		if (!Input.location.isEnabledByUser) {
			Debug.Log ("Nothing get position devices");
			yield break;
		}
		_enabled = false;

		//位置情報の取得に挑戦する。もし、１２０秒経ったら諦める。
		Input.location.Start();
		int maxWait = 120;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds (1);
			maxWait--;
		}
		if (maxWait < 1) {
			Debug.Log ("timed out");
			yield break;
		}

		//デバイスの位置を決定することができません。
		if (Input.location.status == LocationServiceStatus.Failed) {
			Debug.Log ("Unable to detarmine device location");
			yield break;
			//位置を特定しました。
		} else {
			Debug.Log ("Success Load My location");
			_latitude = Input.location.lastData.latitude;
			_longitude = Input.location.lastData.longitude;
		}

		Debug.Log ("Finished get position");
		//取得終了
		Input.location.Stop ();
		_getNow = false;
	}

}
