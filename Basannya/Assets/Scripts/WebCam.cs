﻿using UnityEngine;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;

public class WebCam : MonoBehaviour {
	public int Width = 640; //解像度横
	public int Height = 480; //解像度縦
	public int FPS = 20;
	private WebCamTexture webcamTexture;
	const string temporaryScreenShotPath = "src.png";

	// Import plugin about screen shot
	[DllImport("Shot")]
	private static extern void _PlaySystemShutterSound ();
	[DllImport("Shot")]
	private static extern void _WriteImageToAlbum (string path);

	void Start () {
		WebCamDevice[] devices = WebCamTexture.devices;
		webcamTexture = new WebCamTexture(devices[0].name, Width, Height, FPS);
		GetComponent<Renderer> ().material.mainTexture = webcamTexture;
		//webcamTexture.Play();
	}

	string TemporaryScreenShotPath(){
		return Application.persistentDataPath + "/" + temporaryScreenShotPath;
	}

	public void ScreenShot(){
		
		string format = "yyyy-MM-dd-HH-mm-ss";
		string fileName = System.DateTime.Now.ToString (format) + ".png";
		string filePath = "";

		//スクリーンショット撮影(保存先はApplication.persistentDataPath直下)
		Application.CaptureScreenshot(fileName);

		switch (Application.platform) {
		case RuntimePlatform.IPhonePlayer:
			filePath = Application.persistentDataPath + "/" + fileName;
			_PlaySystemShutterSound ();

			break;
		case RuntimePlatform.Android:
			filePath = Application.persistentDataPath + "/"+fileName;
			//ファイルの保存を待つ処理
			StartCoroutine (ImageCheck(filePath));
			break;
		default:
			filePath = fileName;
			//ファイルの保存を待つ処理
			StartCoroutine (ImageCheck(filePath));
			break;
		}
	}

	IEnumerator ImageCheck(string _filePath){
		while (File.Exists (_filePath) == false) {
			yield return new WaitForSeconds(0.2f); //
		}
		Debug.Log("OK");
	}

	// スクリーンショットの書き込みが終了するまで、毎フレームファイルの有無を確認する。
	/*IEnumerator WaitUntilFinishedWriting (Action callback) {
		while (!System.IO.File.Exists (TemporaryScreenShotPath ())) {
			Debug.Log (">>>>> Temporary Screenshot have not been written yet.");
			yield return null;
		}
		Debug.Log (">>>> Temporary Screenshot have been Written.");
		callback ();
		yield break;
	}*/
}
