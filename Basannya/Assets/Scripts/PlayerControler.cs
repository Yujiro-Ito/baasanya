﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerControler : MonoBehaviour {
	private GameObject _partner;
	private bool _getNow = false;
	private Text _text;
	private string _myName;
	private bool _link = false;

	public GameObject Partner{ get{ return _partner; }}

	// Use this for initialization
	void Start () {
		int rand = (int)Random.Range (0, 10000);
		this.gameObject.name += rand.ToString();
		_text = GameObject.Find ("Canvas/Text").GetComponent<Text> ();
		_myName = this.gameObject.name;
	}
	
	// Update is called once per frame
	void Update () {
		if (_getNow == false && _partner == null) {
			StartCoroutine ("GetObject");
		}

		_text.text = _link.ToString ();
	}

	IEnumerator GetObject(){
		_getNow = true;

		//パートナーのオブジェクト取得
		GameObject[] obj = GameObject.FindGameObjectsWithTag ("Player");
		for (int i = 0; i < obj.Length; i++) {
			if (obj [i].name != _myName) {
				Debug.Log ("MyName is: " + _myName + "Partner Name Is: " + obj [i].name);
				_partner = obj [i];
				yield break;
			}
		}

		//少し待って、繰り返し可能にする。
		yield return new WaitForSeconds(1f);
		_getNow = false;
	}

	public void changeLink(){
		GameObject[] obj = GameObject.FindGameObjectsWithTag ("Player");
		foreach (GameObject j in obj) {
			j.GetComponent<PlayerControler> ().changeSignal();
		}
	}

	void changeSignal(){
		_link = !_link;

	}
}
