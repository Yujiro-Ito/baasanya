﻿using UnityEngine;
using System.Collections;
public class MiniMap : MonoBehaviour {
	GUITexture mapGuiTexture;
	private float intervalTime = 0.0f;
	private int width = 480;
	private int height = 640;
	private double longitude;
	private double latitude;
	private PlayerControler _playerControler;
	private GameObject _partner;
	private bool _imMine = false;
	public bool ImMine{ get{ return _imMine; } set{ _imMine = value; }}


	private int zoom = 16;
	void Start () {
		if (_imMine == true) {
			GetMap ();
		}
		mapGuiTexture = this.GetComponent<GUITexture> ();
		GetPos ();
		_playerControler = GetComponent<PlayerControler> ();
	}

	void Update(){
		if (_partner == null) {
			_partner = _playerControler.Partner;
		}

		//毎フレーム読んでると処理が重くなるので、3秒毎に更新
		intervalTime += Time.deltaTime;
		if (intervalTime >= 3.0f) {
			GetPos ();
			intervalTime = 0.0f;
		}

		if (_imMine == true) {
			GetMap ();
		}

	}

	void GetPos () {
		Debug.Log ("ghaiguhaagl");
		//GPSで取得した緯度経度を変数に代入
		StartCoroutine (GetGPS());
	}

	void GetMap () {
		//マップを取得
		if (_partner != null) {
			_partner.GetComponent<MiniMap>().StartCoroutine (GetStreetViewImage (latitude, longitude, zoom));
		}
	}


	private IEnumerator GetGPS() {
		if (!Input.location.isEnabledByUser) {
			yield break;
		}

		Input.location.Start();
		int maxWait =  120;
		while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0) {
			yield return new WaitForSeconds(1);
			maxWait--;
		}
		if (maxWait < 1) {
			print("Timed out");
			yield break;
		}
		if (Input.location.status == LocationServiceStatus.Failed) {
			print("Unable to determine device location");
			yield break;
		} else {
			print("Location: " + 
				Input.location.lastData.latitude + " " + 
				Input.location.lastData.longitude + " " + 
				Input.location.lastData.altitude + " " + 
				Input.location.lastData.horizontalAccuracy + " " + 
				Input.location.lastData.timestamp);

			longitude = Input.location.lastData.longitude;
			latitude = Input.location.lastData.latitude;
		}
		Input.location.Stop();
	}


	private IEnumerator GetStreetViewImage(double latitude, double longitude, double zoom) {
		//現在地マーカーはここの「&markers」以下で編集可能
		Debug.Log("MyNameIs: " + this.gameObject.name + "PartnerNameIs: " + _partner.name + " Latitude: " + latitude + " Latitude " + longitude);
		string url = "http://maps.googleapis.com/maps/api/staticmap?center=" + latitude + "," + longitude + "&zoom=" + zoom + "&size=" + width + "x" + height + "&markers=size:mid%7Ccolor:red%7C" + latitude + "," + longitude;
		WWW www = new WWW(url);
		yield return www;
		//マップの画像をTextureとして貼り付ける
		mapGuiTexture.texture = www.texture;
		//ミニマップに透明度を与える
		mapGuiTexture.color = new Color (mapGuiTexture.color.r, mapGuiTexture.color.g, mapGuiTexture.color.b, 0.4f);
	}
}
