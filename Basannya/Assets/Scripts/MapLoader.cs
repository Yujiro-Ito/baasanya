﻿using UnityEngine;
using System.Collections;

public class MapLoader : MonoBehaviour {
	[SerializeField]
	GameObject _myObject;
	private MiniMap _map;
	

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		//とりあえず、プレイヤー探そか
		if (_myObject == null) {
			_myObject = GameObject.FindWithTag ("Player");
		}

		//プレイヤーおるんなら、マップの情報もらおか。
		if (_myObject != null && _map == null) {
			_map = _myObject.GetComponent<MiniMap> ();
		}

		//マップの情報取れたなら、ちょっといじろか。
		if(_map != null){
			_map.ImMine = true;
		}
	}
}
